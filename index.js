
/*
//---primitiv-type---
let name = 'amirhossein'
name = 'amir'
let num = 12
let massage = "hello world"
const name ='amirhossein'
*/
//-----refernce type-----
//--object---
/*
let person = {
    name = 'amir',
    age = '22'
}
*/
//----Arrey---
/*
let selectedColor = ['red','bule','green'];
console.log(selectedColor(2));
*/
//----function----
/*
function great(name , family){
    console.log('hello'+' '+ name + ' ' + family)
}
great('amir' , 'khazraiy');
*/
//-----comparison Operators------
/*
let x = 1 ;
let y = '1' ;
//console.log(x == y);
console.log(x !== y);
*/
//-----if---else---
/*
let a = 20;
let b = 10;
if(a > b) {
    console.log('yes');
}else if (a == b){
    console.log('yes*');
}else{
    console.log('no');
}
*/
//---------switch -----case----
/*
let a = 10;
switch (a){
    case 1:
    case 2:
    console.log('02');
    break;
    default:
        console.log('nan');
        break;
}
*/
//------for - loop-------
/*
let i =0;
let a = [1,2,3,4,5,6,7,8,9]
for (i; i < a.length; i++) {
   if (a[i] % 2 === 0){
   console.log('this is even ==>', a[i] );
    }
}
*/

//------for -- in--
/*
let studentArry = ['amir' , 'hasan' , 'ali' , 'yaser', 'mansor']
for (const key in studentArry){
    console.log('the student name is', studentArry[key] )
}
*/
//------ while loop------
/*
let i = 0;
do {
  i += 1;
  console.log(i);
} while (i < 5);
*/

// Constructor function for Person objects
/*
function Person(first, last, age, eye) {
  this.firstName = first;
  this.lastName = last;
  this.age = age;
  this.eyeColor = eye;
  this.name = function() {
    return this.firstName + " " + this.lastName
  };
}

// Create a Person object
var myFather = new Person("John", "Doe", 50, "blue");

// Display full name
document.getElementById("demo").innerHTML =
"My father is " + myFather.name();
*/
/*
function myDisplayer(some) {
    document.getElementById("demo").innerHTML = some;
  }
  
  let myPromise = new Promise(function(myResolve, myReject) {
    let x = 2;
  
  // some code (try to change x to 5)
  
    if (x == 0) {
      myResolve("OK");
    } else {
      myReject("Error");
    }
  });
  
  myPromise.then(
    function(value) {myDisplayer(value);},
    function(error) {myDisplayer(error);}
  );
*/
//-------------Array.map()------
var numbers1 = [45, 4, 9, 16, 25];
var numbers2 = numbers1.map(myFunction);

document.getElementById("demo").innerHTML = numbers2;

function myFunction(value, index, array) {
  return value * 2;
}