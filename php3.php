<?php

//---------while------
 /*
 $number =1;

  while($number <= 10){
      echo "hello" ."<br>";
      $number ++;
    }
    */
//--------do--while-----'
    /*
    $number =1;
    do {
      echo "hello" ."<br>";
      $number++;
    }while ($number <= 10);
*/
//-------for-----
/*
for ($i = 1 ; $i <= 10 ; $i++){
  echo "hello" . "<br>";

}
*/
//---------function--------
/*
function show_message() {
  echo " hello world";
}
show_message();
*/
//-----------
/*
function calculate_sum() {
  $first = 10;
  $second = 15;
  $sum = $first + $second;
  return $sum;
}
 $result = calculate_sum();
 echo $result;
 */
//------------
/*
function calculate_sum($first , $second) {
  $sum = $first + $second;
  return $sum;
}
 $result = calculate_sum(10 , 20);
 echo $result;
 */

 //-------call by value-----
 
 /*
 function increment ($x , $y){
   $x++;
   $y++;
 }
 $a = 3;
 $b = 5;
 echo "a = " . $a . " , " . "b = " . $b . "<br>";
 increment ($a , $b);
 echo "a = " . $a . " , " . "b = " . $b ;
 */
//-------call by reference---
/*
function factorial($number) {
  if($number == 1) return 1;
  return $number * factorial($number - 1);
}
  echo factorial(5);
  */
  
//---------class---object---
//---------public----private---


  /*
  class time {
    private $hour;
    private $min;
    private $sec;
    public function set_time($h, $m, $s){
      $this -> hour = $h;
      $this -> min = $m;
      $this -> sec = $s;
    }
    public function print_time() {
      echo $this -> hour . " : " .  $this -> min . " : " . $this -> sec   ;
    }
  }
  $time1 = new time();
  $time1 -> set_time(10 , 20 , 30);
  $time1 -> print_time();
*/

//--------

/*
class time {
  private $hour;
  private $min;
  private $sec;
  public function __construct($h, $m ,$s){
    $this -> hour = $h;
    $this -> min = $m;
    $this -> sec = $s;
  }
  public function print_time() {
    echo $this -> hour . " : " .  $this -> min . " : " . $this -> sec   ;
  }
}
$time1 = new time(10,11,22);
$time1 -> print_time();
*/


//-----class---inheritance----

/*
class parent_class{
  public function public_massage(){
    echo "this is public massage from parent". "<br>";
  }
  private function private_massage(){
    echo "this is private massage from parent". "<br>";
  }
}
class child_class extends parent_class{

}
$child = new child_class();
$child -> public_massage();
*/

//---------
/*
class parent_class{
 public $public_member =10;
 private $private_member =20;
 protected $protected_member =30;
}
class child_class extends parent_class{
public function __constant(){
echo $this -> public_member . "<br>";
echo $this -> private_member . "<br>";
echo $this -> protected_member . "<br>";
  }
}
$child = new child_class();

*/
